require 'test/unit'
require 'selenium-webdriver'

class MyTest < Test::Unit::TestCase

  def setup

    #define driver for firefox webdriver
    @driver=Selenium::WebDriver.for :firefox
    #maximize the window
    @driver.manage.window.maximize

    #navigate to the test site and clicks login
    @driver.navigate.to "https://flipkart.com"
    @driver.find_element(:xpath, "//div[@class='AsXM8z']//li[@class='_2sYLhZ'][text()='Log In']").click
  end

  def test_login
    #define userName field element	
    loginUserName=@driver.find_element(:xpath, "//div[@class='_39M2dM'])
    #input username
    loginUserName.send_keys('  ')

    #define password field element
    loginPassword=@driver.find_element(:xpath, "//div[@class='_39M2dM'])
    #input password
    loginPassword.send_keys(' ')

    #define submit button element
    loginSubmitButton=@driver.find_element(:xpath, "//div[@class='_1avdGP'])
    #click on submit button
    loginSubmitButton.click

    #wait until the Logout link displays, timeout in 10 seconds
    wait = Selenium::WebDriver::Wait.new(:timeout => 10) # seconds
    wait.until {@driver.find_element(:link_text => "Logout") }

    #define logout link
    logoutLink=@driver.find_element(:xpath, "//div[@class='AsXM8z']//li[@class='_2k0gmP'][text()='Log Out']")
    #if logout link is displayed
    isLogoutLinkDisplayed=logoutLink.displayed?

    puts isLogoutLinkDisplayed

    #verify invalid user error message
    errorMessageValidation = @driver.find_element(:xpath, "//div[@class='_39M2dM']//span[@class='ZAtlA-']).text.include? "Please enter valid Email ID/Mobile number"
     
    puts errorMessageValidation

    #do assertion on logout link
    assert_equal(true,isLogoutLinkDisplayed,'logout button display', errorMessageValidation)

  end

  def teardown
    #quit the driver
    @driver.quit
  end

end